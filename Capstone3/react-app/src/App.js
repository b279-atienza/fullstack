import './App.css';
import AppNavbar from "./components/AppNavbar";
import ProductView from "./components/ProductView"
import OrderView from './components/OrderView';
import UserProfile from './components/UserProfile';
import Smartphones from './components/CategorySmartphones';
import Laptops from './components/CategoryLaptops';
import Smartwatches from './components/CategorySmartwatches';
import Headphones from './components/CategoryHeadphones';
import GamingConsoles from './components/CategoryGamingConsoles';

import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Error from './pages/Error';
import ChangePassword from './pages/ChangePassword';
import Products from './pages/Products';
import OrderManagement from './pages/OrderManagement';
import UserManagement from './pages/UserManagement';
import ProductManagement from './pages/ProductManagement';
import Orders from './pages/Orders';


// npm install react-router-dom
import { Route, Routes } from "react-router-dom"
import { BrowserRouter as Router } from "react-router-dom"
import { Container } from "react-bootstrap";
import { useState, useEffect } from "react"
import { UserProvider } from "./UserContext"

function App() {

const [user, setUser] = useState({
  id: null,
  isAdmin: null,
  email: null,
  token: localStorage.getItem("token")
})

// Function for clearing the storage on logout
const unsetUser = () => {
  localStorage.clear();
}

useEffect(() => {
  localStorage.setItem('email', user.email);
  localStorage.setItem('token', user.token);
}, [localStorage]);

return (
  <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar/>
        <Container fluid>
            <Routes>
                <Route path="/" element={<Home/>}/>
                <Route path="/login" element={<Login/>}/>
                <Route path="/logout" element={<Logout/>}/>
                <Route path="/register" element={<Register/>}/>
                <Route path="/users/changepassword" element={<ChangePassword/>}/>
                <Route path="*" element={<Error/>}/>
                <Route path="/products" element={<Products/>}/>
                <Route path="/ordermanagement" element={<OrderManagement/>}/>
                <Route path="/usermanagement" element={<UserManagement/>}/>
                <Route path="/productmanagement" element={<ProductManagement/>}/>
                <Route path="/orders" element={<Orders/>}/>
                <Route path="/orders/myOrders" element={<OrderView/>}/>
                <Route path="/productView/:productId" element={<ProductView/>}/>
                <Route path="/users/details" element={<UserProfile/>}/>
                <Route path="/Smartphones" element={<Smartphones/>}/>
                <Route path="/Laptops" element={<Laptops/>}/>
                <Route path="/Smartwatches" element={<Smartwatches/>}/>
                <Route path="/Headphones" element={<Headphones/>}/>
                <Route path="/GamingConsoles" element={<GamingConsoles/>}/>
            </Routes>
        </Container>
    </Router>
  </UserProvider>
);
}

export default App;
