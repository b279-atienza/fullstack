import { useState, useEffect } from "react";
import { Container, Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { Navigate } from "react-router-dom";
import { Table } from "@mantine/core";
import { Button } from "@mantine/core";
import { useNavigate } from "react-router-dom";

export default function OrderView() {
  const [orders, setOrders] = useState([]);
  const [totalProducts, setTotalProducts] = useState(0);
  const [totalPrice, setTotalPrice] = useState(0);
  const navigate = useNavigate();

  useEffect(() => {
    const fetchOrdersAndCalculateTotals = async () => {
      try {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/orders/myOrders`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        });

        if (!response.ok) {
          throw new Error(response.status);
        }

        const data = await response.json();
        setOrders(data);
      } catch (error) {
        console.error(error);
        Swal.fire({
          title: "Something Went Wrong!",
          icon: "error",
          text: "Please try again.",
        });
      }
    };

    fetchOrdersAndCalculateTotals();
  }, []);

  useEffect(() => {
    let productsCount = 0;
    let priceSum = 0;
    orders.forEach(({ products, totalAmount }) => {
      productsCount += products.reduce((acc, { quantity }) => acc + quantity, 0      );
      priceSum += totalAmount;
    });
    setTotalProducts(productsCount);
    setTotalPrice(priceSum);
  }, [orders]);

  const handleDelete = async (orderId) => {
    try {
      const result = await Swal.fire({
        title: "Are you sure?",
        text: "This order will be permanently deleted!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d33",
        cancelButtonColor: "#3085d6",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "Cancel",
      });

      if (result.isConfirmed) {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/orders/${orderId}/delete`, {
          method: "DELETE",
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        });

        if (!response.ok) {
          throw new Error(response.status);
        }

        setOrders(orders.filter(order => order._id !== orderId));
        Swal.fire({
          title: "Deleted!",
          text: "The order has been deleted.",
          icon: "success",
        });
      }
    } catch (error) {
     console.error(error);
      Swal.fire({
        title: "Something Went Wrong!",
        icon: "error",
        text: "Please try again.",
      });
    }
  };

  const handleCheckout = () => {
    // Show the SweetAlert
    Swal.fire({
      title: "Checkout Successful!",
      icon: "success",
      text: "Thank you for your purchase.",
    }).then(() => {
		  // Navigate to ProductView page
		  navigate(`/products/`);
		});
    
    
  };

  return (
    <Container>
      <Card className="mt-4">
        <Card.Header>
          <h3>My Orders</h3>
        </Card.Header>
        <Card.Body>
          <Table striped bordered hover responsive className="custom-table">
            <thead>
              <tr>
                <th>Order ID</th>
                <th>Date</th>
                <th>Product</th>
                <th>Quantity</th>
                <th>Total Price</th>
              </tr>
            </thead>
            <tbody>
              {orders.map(({ _id, purchasedOn, totalAmount, products, status }) => (
                <tr key={_id}>
                  <td>{_id}</td>
                  <td>{new Date(purchasedOn).toLocaleDateString()}</td>
                  <td>
                    {products.map(({ product, name }) => (
                      <div key={product._id}>
                        <p>{name}</p>
                      </div>
                    ))}
                  </td>
                  <td>
                    {products.map(({ product, quantity }) => (
                      <div key={product._id}>
                        <p>{quantity}</p>
                      </div>
                    ))}
                  </td>
                  <td>₱ {totalAmount ? totalAmount.toFixed(2) : ''}</td>
                  <td>
                  <Button variant="gradient" gradient={{ from: 'orange', to: 'red' }} onClick={() => handleDelete(_id)}>
                      Delete
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
            
            <tfoot>
              <tr>
                <td colSpan="3"></td>
                <td>Total Products: {totalProducts}</td>
                <td>Total Price: ₱{totalPrice.toFixed(2)}</td>
                <td>
                <Button variant="gradient" gradient={{ from: 'teal', to: 'lime', deg: 105 }} onClick={handleCheckout}>
                Checkout All
              </Button>
                </td>
              </tr>
            </tfoot>
          </Table>
        </Card.Body>
      </Card>
    </Container>
  );
}