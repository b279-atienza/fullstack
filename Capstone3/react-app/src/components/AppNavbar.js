import { useEffect, useState, useContext } from "react";
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";
import { UnstyledButton, Group, Avatar, Text } from '@mantine/core';
import UserContext from "../UserContext";

export default function AppNavbar() {
  const { user, setUser } = useContext(UserContext);
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');

  useEffect(() => {
    const storedUsername = localStorage.getItem('username');
    const storedEmail = localStorage.getItem('email');

    if (storedUsername && storedEmail) {
      setUsername(storedUsername);
      setEmail(storedEmail);
    }
  }, []);

  return (
    <Navbar bg="light" expand="lg" className="px-3">
      <Navbar.Brand as={Link} to={"/"} className="fw-bold">
        Digital Depot
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ms-auto">
          {localStorage.isAdmin === 'true' ? (
            <>
              <Nav.Link as={NavLink} to="/UserManagement" end>
              User Management
              </Nav.Link>
              <Nav.Link as={NavLink} to="/ProductManagement" end>
               Product Management
              </Nav.Link>
              <Nav.Link as={NavLink} to="/OrderManagement" end>
               Order Management
              </Nav.Link>
            </>
          ) : (
            <>
              <Nav.Link as={NavLink} to="/" end>
              Home
              </Nav.Link>
              <Nav.Link as={NavLink} to="/products" end>
                Products
              </Nav.Link>
              {localStorage.token && (
                <Nav.Link as={NavLink} to="/orders/myOrders/" end>
                  Cart
                </Nav.Link>
              )}
            </>
          )}
          {localStorage.token ? (
            <>
              <NavDropdown
                title={
                  <UnstyledButton>
                    <Group>
                      <Text color="red">{username}</Text>
                    </Group>
                  </UnstyledButton>
                }
                align="end"
                id="collasible-nav-dropdown"
                end
              >
                <NavDropdown.Item as={NavLink} to="/users/changepassword" end>
                  Change Password
                </NavDropdown.Item>
                <NavDropdown.Item as={NavLink} to="/users/details" end>
                  My Profile
                </NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item as={NavLink} to="/logout" end>
                  Logout
                </NavDropdown.Item>
              </NavDropdown>
            </>
          ) : (
            <>
              <Nav.Link as={NavLink} to={"/register"}>
                Register
              </Nav.Link>
              <Nav.Link as={NavLink} to={"/login"}>
                Login
              </Nav.Link>
            </>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}