import { useEffect, useState} from "react"
import ProductCard from './ProductCard'
import { SimpleGrid } from '@mantine/core';
import Category from './Category';
import "../pages/Pages.css";


export default function GamingConsole(){
	const [AllGamingConsole, setAllGamingConsole] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/category/GamingConsoles`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setAllGamingConsole(data.map(product => {
				return (
					<ProductCard key={product._id} productProp={product}/>
					)
			}))
		})
	}, [])

return(
	<>
		<div className="p-1">
			<div className="category-highlights-container">
				<div className="category-container">
					<Category />
				</div>

				<div className="highlights-container">
					<h1>Gaming Console</h1>
					<SimpleGrid cols={3}>
					{AllGamingConsole}
					</SimpleGrid>
				</div>
			</div>
		</div>
	</>
	)
}