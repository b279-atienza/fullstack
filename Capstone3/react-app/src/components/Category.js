import { Card, Image, Text, Badge, Button, Group } from '@mantine/core';
import { Row, Col, Carousel } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Category() {
  return (
    <Carousel>
  <Carousel.Item>
    <Row className="d-flex justify-content-center flex-column">
      <h1 className="text-center">CATEGORY</h1>
      <Col>
        <Card className="xl-5" shadow="sm" padding="lg" radius="md" withBorder>
          <Card.Section>
            <Image
              src="https://drive.google.com/uc?id=1ePhvegtQrVVRVXT4_nRRliT3xIkoysp6"
              height={160}
              alt="Smartphones"
            />
          </Card.Section>
          <Group position="center" mt="md" mb="xs">
            <Link className="btn btn-primary" to={`/Smartphones`}>
              Smartphones
            </Link>
          </Group>
        </Card>
      </Col>

      <Col>
        <Card className="xl-5" shadow="sm" padding="lg" radius="md" withBorder>
          <Card.Section>
            <Image
              src="https://drive.google.com/uc?id=1CxEJoqzKpUWr0tLMdFI54k5FmXgAh9Lf"
              height={160}
              alt="Laptops"
            />
          </Card.Section>
          <Group position="center" mt="md" mb="xs">
            <Link className="btn btn-primary" to={`/Laptops`}>
              Laptops
            </Link>
          </Group>
        </Card>
      </Col>

      <Col>
        <Card className="xl-5" shadow="sm" padding="lg" radius="md" withBorder>
          <Card.Section>
            <Image
              src="https://drive.google.com/uc?id=1d3iat8SwdEqODBsAVjFlkbVq0_RmxuZA"
              height={160}
              alt="Smartwatches"
            />
          </Card.Section>
          <Group position="center" mt="md" mb="xs">
            <Link className="btn btn-primary" to={`/Smartwatches`}>
              Smartwatches
            </Link>
          </Group>
        </Card>
      </Col>

      <Col>
        <Card className="xl-5" shadow="sm" padding="lg" radius="md" withBorder>
          <Card.Section>
            <Image
              src="https://drive.google.com/uc?id=19_9Ya460qGQ930NDbRlDhOJFFyae47VB"
              height={160}
              alt="Headphones"
            />
          </Card.Section>
          <Group position="center" mt="md" mb="xs">
            <Link className="btn btn-primary" to={`/Headphones`}>
              Headphones
            </Link>
          </Group>
        </Card>
      </Col>

      <Col>
        <Card className="xl-5" shadow="sm" padding="lg" radius="md" withBorder>
          <Card.Section>
          <Image
              src="https://drive.google.com/uc?id=12yFnGtiwtoqCV3MlN4k6erCEZyti6Isz"
              height={160}
              alt="Gaming Consoles"
            />
          </Card.Section>
          <Group position="center" mt="md" mb="xs">
            <Link className="btn btn-primary" to={`/GamingConsoles`}>
              Gaming Consoles
            </Link>
          </Group>
        </Card>
      </Col>

    </Row>
  </Carousel.Item>
</Carousel>
  );
}