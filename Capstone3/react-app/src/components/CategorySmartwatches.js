import ProductCard from './ProductCard'
import { useEffect, useState} from "react"
import { SimpleGrid } from '@mantine/core';
import Category from './Category';
import "../pages/Pages.css";

export default function Smartwatches(){
	const [AllSmartwatches, setAllSmartwatches] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/category/Smartwatches`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setAllSmartwatches(data.map(product => {
				return (
					<ProductCard key={product._id} productProp={product}/>

					)
			}))
		})
	}, [])

	return(
		<>
		<div className="p-1">
			<div className="category-highlights-container">
				<div className="category-container">
					<Category />
				</div>

				<div className="highlights-container">
					<h1>Smartwatches</h1>
					<SimpleGrid cols={3}>
					{AllSmartwatches}
					</SimpleGrid>
				</div>
			</div>
		</div>
		</>
		)
}