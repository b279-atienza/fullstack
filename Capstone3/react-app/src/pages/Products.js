import { useEffect, useState } from "react";
import ProductCard from "../components/ProductCard";
import { SimpleGrid } from '@mantine/core';

function Pagination({ currentPage, totalPages, onPageChange }) {
  const pages = Array.from({ length: totalPages }, (_, i) => i + 1);

  return (
    <nav>
      <ul className="pagination">
        {pages.map((page) => (
          <li
            key={page}
            className={`page-item${page === currentPage ? " active" : ""}`}
          >
            <button className="page-link" onClick={() => onPageChange(page)}>
              {page}
            </button>
          </li>
        ))}
      </ul>
    </nav>
  );
}

export default function Products() {
  const [currentPage, setCurrentPage] = useState(1);
  const [AllProducts, setAllProducts] = useState([]);
  const [totalPages, setTotalPages] = useState(1); // declare totalPages as a state variable

  const PAGE_SIZE = 6;

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products`)
      .then((res) => res.json())
      .then((data) => {
        const totalProducts = data.length;
        const totalPages = Math.ceil(totalProducts / PAGE_SIZE);

        setCurrentPage(1);
        setTotalPages(totalPages); // set the value of totalPages
        setAllProducts(
          data.slice(0, PAGE_SIZE).map((product) => (
            <ProductCard key={product._id} productProp={product} />
          ))
        );
      });
  }, []);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products?page=${currentPage}`)
      .then((res) => res.json())
      .then((data) => {
        const startIndex = (currentPage - 1) * PAGE_SIZE;
        const endIndex = startIndex + PAGE_SIZE;
        const productsForCurrentPage = data.slice(startIndex, endIndex);

        setAllProducts(
          productsForCurrentPage.map((product) => (
            <ProductCard key={product._id} productProp={product} />
          ))
        );
      });
  }, [currentPage]);

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  return (
    <>
      <h1 className="text-center mt-5">PRODUCTS</h1>
      <SimpleGrid cols={3}>
      {AllProducts}
      </SimpleGrid>
      <Pagination
        currentPage={currentPage}
        totalPages={totalPages}
        onPageChange={handlePageChange}
      />
    </>
  );
}