import { useState, useEffect } from "react";
import { Container, Card } from "react-bootstrap";
import { Table, Dropdown, DropdownButton } from "react-bootstrap";
import { Pagination } from '@mantine/core';
import Swal from "sweetalert2";


export default function OrderManagement() {
  const [orders, setOrders] = useState([]);
  const [totalProductPrice, setTotalProductPrice] = useState(0);
  const [uniqueProductNames, setUniqueProductNames] = useState(new Set());
  const [uniqueUserIds, setUniqueUserIds] = useState(new Set());
  const [activePage, setActivePage] = useState(1);

  const PAGE_SIZE = 7;

// compute total number of pages based on number of products and page size
const totalPages = Math.ceil(orders.length / PAGE_SIZE);

const startIdx = (activePage - 1) * PAGE_SIZE;
const endIdx = startIdx + PAGE_SIZE;
const displayedOrder = orders.slice(startIdx, endIdx);

  useEffect(() => {
    const fetchOrders = async () => {
      try {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/orders`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        });

        if (!response.ok) {
          throw new Error(response.status);
        }

        const data = await response.json();
        const totalPrice = data.reduce((acc, { products, userId }) => {
          const productPrices = products.map(({ price, quantity }) => price * quantity);
          const orderTotalPrice = productPrices.reduce((sum, price) => sum + price, 0);
          const newNames = new Set(products.map(({ name }) => name));
          setUniqueProductNames((prevNames) => new Set([...prevNames, ...newNames]));
          setUniqueUserIds((prevIds) => new Set([...prevIds, userId]));
          return acc + orderTotalPrice;
        }, 0);
        setTotalProductPrice(totalPrice);
        setOrders(data);
      } catch (error) {
        console.error(error);
        Swal.fire({
          title: "Something Went Wrong!",
          icon: "error",
          text: "Please try again.",
        });
      }
    };

    fetchOrders();
  }, []);

  const handleProductClick = (name) => {
    const productTotalPrice = orders.reduce((acc, { products }) => {
      const product = products.find((p) => p.name === name);
      if (product) {
        const { price, quantity } = product;
        return acc + price * quantity;
      }
      return acc;
    }, 0);
    Swal.fire({
      title: `Total Price for Product Name ${name}`,
      text: `${productTotalPrice}`,
      icon: "info",
    });
  };

  const handleUserClick = (userId) => {
    const userTotalPrice = orders.reduce((acc, { products, userId: orderUserId }) => {
      if (orderUserId === userId) {
        const productPrices = products.map(({ price, quantity }) => price * quantity);
        const orderTotalPrice = productPrices.reduce((sum, price) => sum + price, 0);
        return acc + orderTotalPrice;
      }
      return acc;
    }, 0);
    Swal.fire({
      title: `Total Price for User ID ${userId}`,
      text: `${userTotalPrice}`,
      icon: "info",
    });
  };

  return (
    <Container>
      <Card className="mt-4">
        <Card.Header className="d-flex justify-content-between align-items-center">
          <h3>Order Management</h3>
          <h3>Total Product Price: {totalProductPrice}</h3>
          <DropdownButton id="dropdown-basic-button" title="Total Prices by Product Name and User ID">
            <Dropdown.Item disabled>Total Prices by Product Name</Dropdown.Item>
            {Array.from(uniqueProductNames).map((name) => (
              <Dropdown.Item key={name} onClick={() => handleProductClick(name)}>
                {name}
              </Dropdown.Item>
            ))}
            <Dropdown.Item disabled>Total Prices by User ID</Dropdown.Item>
            {Array.from(uniqueUserIds).map((userId) => (
              <Dropdown.Item key={userId} onClick={() => handleUserClick(userId)}>
                {userId}
              </Dropdown.Item>
            ))}
          </DropdownButton>
        </Card.Header>
        <Card.Body>
          <Table striped bordered hover responsive className="custom-table">
            <thead>
              <tr>
                <th>Order ID</th>
                <th>Purchase Date</th>
                <th>User Id</th>
                <th>Product Name</th>
                <th>Product Price</th>
                <th>Quantity</th>
                <th>Total Amount</th>
              </tr>
            </thead>
            <tbody>
              {displayedOrder.map(({ _id, purchasedOn, userId, products,totalAmount }) =>
                products.map(({ name, quantity, price }) => (
                  <tr key={`${_id}-${name}`}>
                    <td>{_id}</td>
                    <td>{purchasedOn}</td>
                    <td>{userId}</td>
                    <td>{name}</td>
                    <td>{price}</td>
                    <td>{quantity}</td>
                    <td>{totalAmount}</td>
                  </tr>
                ))
              )}
            </tbody>
          </Table>
        </Card.Body>
      </Card>
      <div className="d-flex justify-content-center">
      <Pagination
        value={activePage}
        onChange={setActivePage}
        total={totalPages}
      />
    </div>
    </Container>
  );
}