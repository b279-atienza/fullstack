import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
import Category from "../components/Category";
import Products from "./Products";
import "./Pages.css"; // Import the CSS file

export default function Home() {
  return (
    <div className="p-1">
      <div className="home-background"></div>
      <div className="category-highlights-container">
        <div className="category-container">
          <Category />
        </div>
        <div className="highlights-container">
          <Highlights />
        </div>
      </div>
      <Products />
    </div>
  );
}